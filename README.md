Mobilizon - Documentation
=========================

This repository contains source code for Mobilizon's documentation website.

Public URL : https://docs.joinmobilizon.org/


# Local Testing

For local testing you can use the following

## Setting up dependecies

```
python -m virtualenv venv
source venv/bin/activate
pip install -r requirements.txt
```

## Run and build

```
# For debugging
mkdocs serve
# For release build
mkdocs build
```
