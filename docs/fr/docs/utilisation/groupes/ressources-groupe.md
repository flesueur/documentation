# Ressources du groupe

Un endroit pour stocker des liens vers des documents ou des ressources de tout type.

La section **Ressources** vous permet d'ajouter&nbsp;:

  * un dossier
  * un lien web
  * un calc
  * un pad
  * une visio-conference

![image listant les ressources possibles](../../images/group-add-ressources-FR.png)

## Accéder aux ressources

Pour accéder aux ressources du groupe, vous devez être **au minimum** membre de ce groupe. Ensuite, vous devez&nbsp;:

  1. cliquer sur **Mes groupes** dans la barre de navigation supérieure
  * cliquer sur le groupe souhaité
  * cliquer sur **Voir tous**

    ![titre de la section ressources](../../images/group-resource-title-FR.png)

## Ajouter une ressource

Pour ajouter une ressource, vous devez, depuis [la page ressource](#acceder-aux-ressources), cliquer sur le bouton **+** et choisir le type de ressource à ajouter&nbsp;:

  ![image listant les ressources possibles](../../images/group-add-ressources-FR.png)

### Ajouter un nouveau dossier

Pour ajouter un nouveau dossier, vous devez, depuis [la page ressource](#acceder-aux-ressources)&nbsp;:

  1. [cliquer sur le bouton **+**](#ajouter-une-ressource) (voir ci-dessus)
  * choisir **Nouveau dossier**
  * indiquer le nom de ce dossier
  * cliquer sur le bouton **Créer un dossier**

### Ajouter un nouveau lien

Pour ajouter un nouveau lien, vous devez, depuis [la page ressource](#acceder-aux-ressources)&nbsp;:

  1. [cliquer sur le bouton **+**](#ajouter-une-ressource) (voir ci-dessus)
  * choisir **Nouveau lien**
  * ajouter le lien web dans le champ **URL**
  * [Optionnel] ajouter un titre (celui-ci sera automatiquement ajouté, sinon)
  * [Optionnel] ajouter une description (celle-ci sera automatiquement ajoutée, sinon)
  * cliquer sur le bouton **Créer une ressource**

### Ajouter un nouveau calc

Pour ajouter un nouveau calc, vous devez, depuis [la page ressource](#acceder-aux-ressources)&nbsp;:

  1. [cliquer sur le bouton **+**](#ajouter-une-ressource) (voir ci-dessus)
  * choisir **Créer un calc**
  * choisir un nom pour ce calc
  * cliquer sur le bouton **Créer un calc**

!!! note
    Ce calc sera créé sur un site externe (configurable par le ou la gestionnaire de l'instance)

### Ajouter un nouveau pad

Pour ajouter un nouveau pad, vous devez, depuis [la page ressource](#acceder-aux-ressources)&nbsp;:

  1. [cliquer sur le bouton **+**](#ajouter-une-ressource) (voir ci-dessus)
  * choisir **Créer un pad**
  * choisir un nom pour ce pad
  * cliquer sur le bouton **Créer un pad**

!!! note
    Ce pad sera créé sur un site externe (configurable par le ou la gestionnaire de l'instance)

### Ajouter une nouvelle visio-conférence

Pour ajouter une nouvelle visio-conférence, vous devez, depuis [la page ressource](#acceder-aux-ressources)&nbsp;:

  1. [cliquer sur le bouton **+**](#ajouter-une-ressource) (voir ci-dessus)
  * choisir **Créer une nouvelle visio-conférence**
  * choisir un nom pour cette visio-conférence
  * cliquer sur le bouton **Créer une visio-conférence**

!!! note
    Cette visio-conférence sera créée sur un site externe (configurable par le ou la gestionnaire de l'instance)

## Renommer une ressource

Pour renommer un dossier ou un fichier vous devez, sur [la page ressource](#acceder-aux-ressources)&nbsp;:

  1. cliquer sur **⋅⋅⋅** sur la ligne de la ressource à modifier
  * cliquer sur <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M20.71,7.04C21.1,6.65 21.1,6 20.71,5.63L18.37,3.29C18,2.9 17.35,2.9 16.96,3.29L15.12,5.12L18.87,8.87M3,17.25V21H6.75L17.81,9.93L14.06,6.18L3,17.25Z" />
    </svg> **Renommer**
  * renommer la ressource
  * cliquer sur le bouton **Renommer la ressource**

## Déplacer une ressource

Vous avez la possibilité de déplacer un fichier dans un dossier ou un dossier dans un autre, etc… Pour ce faire, vous devez&nbsp;:

  1. cliquer sur **⋅⋅⋅** sur la ligne de la ressource
  * cliquer sur <svg style="width:24px;height:24px" viewBox="0 0 24 24"><path fill="currentColor" d="M14,18V15H10V11H14V8L19,13M20,6H12L10,4H4C2.89,4 2,4.89 2,6V18A2,2 0 0,0 4,20H20A2,2 0 0,0 22,18V8C22,6.89 21.1,6 20,6Z" /></svg> **Déplacer**
  * choisir l’endroit où déplacer votre ressource
  * une fois le bon endroit trouvé, cliquez sur le bouton **Déplacer la ressource dans [NOM DU DOSSIER]**

![gif montrant le déplacement d'un fichier dans un dossier](../../images/move-resource-FR.gif)

## Supprimer un ressource

Pour supprimer une ressources, vous devez, sur [la page ressource](#acceder-aux-ressources)&nbsp;:

  1. cliquer sur **⋅⋅⋅** sur la ligne de la ressource
  * cliquer sur <svg style="width:24px;height:24px" viewBox="0 0 24 24">
    <path fill="currentColor" d="M19,4H15.5L14.5,3H9.5L8.5,4H5V6H19M6,19A2,2 0 0,0 8,21H16A2,2 0 0,0 18,19V7H6V19Z" /></svg> **Supprimer**
