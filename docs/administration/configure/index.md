# Configuration

Basic Mobilizon configuration can be handled through the Admin panel in the UI.
Otherwise you need to edit various configuration files, depending on how you installed Mobilizon.

## Releases

Core mobilizon configuration must be managed into the `/etc/mobilizon/config.exs` file.
After performing changes to this file, you have to restart the Mobilizon service:
```
systemctl restart mobilizon
```

## Docker

Basic things can be edited through the `.env` file, but [you can use the `config.exs` file](../install/docker.md#advanced-configuration) linked to a volume to edit & add custom settings.

## Source

Mobilizon configuration must be managed into the `config/runtime.exs` file.
After performing changes to this file, you have to restart the Mobilizon service:
```
systemctl restart mobilizon
```
